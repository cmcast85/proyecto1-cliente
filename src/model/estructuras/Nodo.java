package model.estructuras;


public class Nodo<T> {
	
	private T objeto;
	private Nodo<T> siguiente;
	private Nodo<T> anterior;
	private boolean dobleEncadenada;
	
	public Nodo(T obj) {
		objeto = obj;
		siguiente = null;
		anterior = null;
		dobleEncadenada = false;
	}
	
	public Nodo(T obj, Nodo<T> iAnterior) {
		objeto = obj;
		siguiente = null;
		anterior = iAnterior;
		dobleEncadenada = true;
	}

	public void cambiarSiguiente(Nodo<T> iSiguiente) {
		siguiente = iSiguiente;
	}
	
	public void cambiarAnterior(Nodo<T> iAnterior) {
		if (dobleEncadenada) anterior = iAnterior;
	}
	
	public Nodo<T> darSiguiente() {
		return siguiente;
	}
	
	public Nodo<T> darAnterior() {
		return anterior;
	}
	
	public T darObjeto() {
		return objeto;
	}
}
